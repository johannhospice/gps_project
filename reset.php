<?php require_once 'inc/model/bootstrap.php';

if (isset($_GET['id']) && isset($_GET['token'])) {
    $id = $_GET['id'];
    $db = App::getDB();
    if ($user = App::getUser()->resetPasswordConfirmTokenPermission($db, $id, $_GET['token'])) {
        if (!empty($_POST)) {
            $validator = new Validator($_POST);
            $validator->isPassword('password', 'Les mots de passe sont incohérents');
            if ($validator->isValid()) {
                $password = Str::password_hash($_POST['password']);
                $db->query("update users set password = '$password', reset_at=null, reset_token = null where id=$id");
                Session::instance()->setFlash('success', 'Votre mot de passe a bien été modifié');
                App::redirect('account.php', true);
            }
            Session::instance()->setFlash('danger', 'Votre mot de passe n\'est pas valide');
        }
    } else {
        Session::instance()->setFlash('danger', 'Ce token n\'est pas valide');
        App::redirect('login.php', true);
    }
} else
    App::redirect('login.php', true);

include("inc/header.php"); ?>

    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 center">
                    <h2>Mot de passe oublié</h2>
                    <p>Veillez saisir votre nouveau mot de passe connecter à votre compte GPS.</p>

                    <form class="form row" action="" method="POST" role="form">
                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="password" name="password" id="input-field"
                                       required>
                                <label for="lastname" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">lock</i>
                                    <span class="input_label-content" id="input_label-content">Mot de passe</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="password" name="password_confirm" id="input-field"
                                       required>
                                <label for="lastname" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">lock_outline</i>
                                    <span class="input_label-content" id="input_label-content">Confirmation du mot de passe</span>
                                </label>
                            </div>
                        </div>
                        <input type="submit" value="Confirmer" class="btn submit-form"/>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include('inc/footer.php') ?>