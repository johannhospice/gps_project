<?php require_once 'inc/model/bootstrap.php';
if (App::getUser()->user())
    App::redirect("account.php", true);
include("inc/header.php");
?>

    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 center">
                    <h2>Mot de passe oublié</h2>
                    <p>Pour réinitialiser votre mot de passe, saisissez l'adresse e-mail que vous utilisez pour vous
                        connecter à votre compte GPS.</p>

                    <form class="form row" action="inc/controller/forget.php" method="POST" role="form">

                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="email" name="email" id="input-field"
                                       required>
                                <label for="lastname" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">mail</i>
                                    <span class="input_label-content" id="input_label-content">Email</span>
                                </label>
                            </div>
                        </div>

                        <input type="submit" value="Envoyer" class="btn"/>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include('inc/footer.php') ?>