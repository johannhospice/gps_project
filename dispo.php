<?php require_once 'inc/model/bootstrap.php';
$user = App::getUser();
if (!$user_info = $user->user()) $user->restrict();

if (!empty($_POST) && !empty($_POST['start']) && !empty($_POST['end']) && !empty($_POST['com'])) {
    $db = App::getDB();
    $validator = new Validator($_POST);/*
    $validator->isMatch('start', Validator::PREG_DATE, "Date invalide");
    $validator->isMatch('end', Validator::PREG_DATE, "Date invalide");
*/
    $com = $validator->secureText(App::getDB(), 'com');
    var_dump($_POST['end']);
    if ($validator->isValid())
        if ($user->disposition($db, $user_info->id, $_POST['start'], $_POST['end'], $com)) {
            Session::instance()->setFlash('success', 'Vos disposition on été correctement enregistrées');
            App::redirect('account.php');
        } else
            Session::instance()->setFlash('danger', 'Probleme d\'enregistrement de vos disposition');
    foreach ($validator->getErrors() as $error)
        Session::instance()->setFlash("danger", $error);

}
include("inc/header.php"); ?>

    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 center">
                    <h1>Disponibilitées</h1>
                    <p>Saisissez vos disponibilitées pour les soutenances de stage.</p>

                    <form class="form row" action="" method="POST" role="form">

                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="date" name="start" id="input-field"
                                       required>
                                <label for="date" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">date_range</i>
                                </label>
                                <span class='input_info'>Du</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="date" name="end" id="input-field"
                                       required>
                                <label for="date" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">date_range</i>
                                </label>
                                <span class='input_info'>Au</span>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="com" id="textarea_field "
                                ></textarea>
                                <label for="msg" class="textarea_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">mail_outline</i>
                                <span class="input_label-content disable"
                                      id="input_label-content">Commentaire</span>
                                </label>
                            </div>
                        </div>

                        <input type="submit" value="Envoyer" class="btn "/>
                    </form>
                </div>
            </div>
        </div>
    </section>


<?php include("inc/footer.php"); ?>