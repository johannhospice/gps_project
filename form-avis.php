<?php require_once 'inc/model/bootstrap.php';
$user = App::getUser();
$user_info = $user->user();
if (!($user_info->type == 0 || $user_info->type == 1))
    $user->restrict();

include("inc/header.php");
?>
    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center">

                    <h1>Votre avis sur le stage</h1>

                    <form class="form row" action="" method="POST" role="form">
                        <h2>Tuteur</h2>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="firstname_tutor" id="input-field"
                                       required>
                                <label for="firstname_tutor" class="input_label">
                                    <span class="input_label-content" id="input_label-content">Prenom du tuteur</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="lastname_tutor" id="input-field"
                                       required>
                                <label for="lastname_tutor" class="input_label">
                                    <span class="input_label-content" id="input_label-content">Nom du tuteur</span>
                                </label>
                            </div>
                        </div>



                        <h2> Remunération </h2>
                        <div class="col-sm-6">
                            <div class="input_group ">
                                <select class="input_field "
                                        name="rem" required>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                <label for="status" class="input_label">
                                    <span class="input_label-content-active">Rémunération</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="rem_value" id="input-field"
                                       required>
                                <label for="name_resp" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Combien en €</span>
                                </label>
                            </div>
                        </div>

                        <h2>Encadrement</h2>
                        <div class="col-sm-12">
                            <div class="input_group ">
                                <select class="input_field "
                                        name="rem" required>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                <label for="status" class="input_label">
                                    <span class="input_label-content-active">Avez-vous été encadré directement par un informaticien</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group ">
                                <select class="input_field "
                                        name="rem" required>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                <label for="status" class="input_label">
                                    <span class="input_label-content-active">En cas de besoin pouviez-vous faire appel à un informaticien</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group ">
                                <select class="input_field "
                                        name="rem" required>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                <label for="status" class="input_label">
                                    <span class="input_label-content-active">Dans le cadre de votre stage,avez-vous travaillé seul ?</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="text" name="rem_value" id="input-field"
                                       required>
                                <label for="name_resp" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Taille de l'équipe</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <fieldset>
                                    <legend>Taille de l'équipe</legend>
                                    <div>
                                        <input type="checkbox" value="0">PC</input>
                                        <input type="text" placeholder="AUTRES"/>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <fieldset>
                                    <legend>Système</legend>
                                    <div>
                                        <input type="checkbox" value="0">UNIX</input>
                                        <input type="checkbox" value="1">LINUX</input>
                                        <input type="checkbox" value="2">WINDOWS</input>
                                        <input type="text" placeholder="AUTRES"/>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                                <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Langages</span>
                                </label>
                            </div>
                        </div>
                        <h2>Objet principal du stage</h2>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <fieldset>
                                    <legend>Système</legend>
                                    <div>
                                        <input type="checkbox" value="0">Systeme</input>
                                        <input type="checkbox" value="1">Multimedia</input>
                                        <input type="checkbox" value="2">Réseaux</input>
                                        <input type="checkbox" value="3">Développement WEB</input>
                                        <input type="checkbox" value="4">Aute Développement</input>
                                        <input type="checkbox" value="5">Bases de données</input>
                                        <input type="checkbox" value="5">Autres..</input>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <h2>Le stage obligatoire De Fin D'études Doit Répondre À Plusieurs Objectifs</h2>
                        <p> D'abord, il vous introduit dans le monde du travail, dans une ambiance que le futur
                            professionnel de
                            l'Informatique doit connaître avec ses contraintes de temps, de budget, de fonctionnement
                            d'équipe, etc...
                            Il vous permet d'être confronté, non plus à des exercices scolaires dont l'intérêt est
                            souvent purement
                            pédagogique, mais à des applications concrètes dans les domaines les plus variés.<br>
                            Il vous permet, soit d'approfondir les connaissances acquises à l'IUT en étant confronté à
                            des problèmes
                            en vraie grandeur, soit de découvrir des environnements de travail, des méthodes d'analyse,
                            des langages
                            nouveaux.</p>
                        <div class="col-sm-12">
                            <div class="input_group ">
                                <select class="input_field "
                                        name="rem" required>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                <label for="status" class="input_label">
                                    <span class="input_label-content-active">Dans le cadre de votre stage,avez-vous travaillé seul ?</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                                <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Après cette expérience dans l’entreprise, estimez-vous que certaines matières enseignées n’ont pas été assez développées ?</span>
                                </label>
                            </div>
                        </div>
                        <h2>Avis De l’etudiant Sur Les enseignements Dispenses A L’IUT</h2>
                        <div class="col-sm-12">
                            <div class="input_group ">
                                <select class="input_field "
                                        name="rem" required>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                <label for="status" class="input_label">
                                    <span class="input_label-content-active">Dans le cadre de votre stage,avez-vous travaillé seul ?</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                                <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Après cette expérience dans l’entreprise, estimez-vous que certaines matières enseignées n’ont pas été assez développées ?</span>
                                </label>
                            </div>
                        </div>

                        <h2>Apport du stage dans votre projet perdonnel et professionnel</h2>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                                <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Précisez en quelques lignes comment le stage a enrichi ou modifié votre projet personnel et professionnel</span>
                                </label>
                            </div>
                        </div>

                        <h2>Fichier annexes</h2>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="file" name="file" id="input-field"
                                       >
                                <label for="msg" class="textarea_label">
                                </label>
                            </div>
                        </div>
                        <input type="submit" value="soumettre" class="btn"/>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include('inc/footer.php') ?>