<?php require_once 'inc/model/bootstrap.php';
$user = App::getUser();
$user_info = $user->user();
if (!($user_info->status == App::STUDENT))     $user->restrict();

include("inc/header.php");
?>

    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center">
                    <h1>Localisation du stage</h1>
                    <form class="form row" action="" method="POST" role="form">
                        <h2>Entreprise</h2>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="text" name="name_ent" id="input-field"
                                       required>
                                <label for="name_ent" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">work</i>
                                    <span class="input_label-content" id="input_label-content">Raison sociale de l'entreprise</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="text" name="address_ent" id="input-field"
                                       required>
                                <label for="address_ent" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">place</i>
                                    <span class="input_label-content"
                                          id="input_label-content">Adresse de l'entreprise</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="name_ent" id="input-field"
                                       required>
                                <label for="name_ent" class="input_label">
                                    <span class="input_label-content" id="input_label-content">Code postale de l'entreprise</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="name_ent" id="input-field"
                                       required>
                                <label for="name_ent" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Ville de l'entreprise</span>
                                </label>
                            </div>
                        </div>

                        <h2>Responsable</h2>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="firstname_resp" id="input-field"
                                       required>
                                <label for="name_ent" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">account_circle</i>
                                    <span class="input_label-content"
                                          id="input_label-content">Prenom du responsable</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="lastname_resp" id="input-field"
                                       required>
                                <label for="name_resp" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Nom du responsable</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="text" name="func_resp" id="input-field"
                                       required>
                                <label for="name_ent" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Fonction du responsable</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="email_resp" id="input-field"
                                       required>
                                <label for="firstname_tutor" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">mail_outline</i>
                                    <span class="input_label-content" id="input_label-content">Email</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="email_resp" id="input-field"
                                       required>
                                <label for="firstname_tutor" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">phone</i>
                                    <span class="input_label-content"
                                          id="input_label-content">Numero de téléphone</span>
                                </label>
                            </div>
                        </div>

                        <h2>Vous</h2>
                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="text" name="email_resp" id="input-field"
                                       required>
                                <label for="firstname_tutor" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">phone</i>
                                    <span class="input_label-content" id="input_label-content">Numero de téléphone où l'on peut vous joindre en entreprise</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                                <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Sujet du stage</span>
                                </label>
                            </div>
                        </div>
                        <input type="submit" value="soumettre" class="btn "/>

                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include('inc/footer.php') ?>