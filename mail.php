<?php require_once 'inc/model/bootstrap.php';
if (isset($_GET['email'])) {
    $mailbox = new Mail($_GET['email']);
    $mailbox->mailsToCookie(App::getDB(), Cookie::instance());
    App::redirect('mail.php', true);
}
?>
<head>
    <link rel="stylesheet" href="css/responsive.css">
    <style>
        * {
            text-align: center;
            font-family: Arial, serif;
        }

        section {
            margin: 25px;
            border: 1px solid black;
        }
    </style>
</head>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1>Boite Mail</h1>
            <?php if (Cookie::instance()->hasCookie('receiver')): ?>
                <h2><?= Cookie::instance()->getCookie('receiver') ?></h2>
                <?php foreach (Cookie::instance()->getCookie('mail') as $id => $msg): ?>
                    <?php $mail = Mail::mailUnformatter($msg); ?>
                    <section>
                        <div>
                            <p><?= $mail['se']; ?> - <?= $mail['da']; ?></p>
                        </div>
                        <div>
                            <p><b><?= $mail['ti']; ?></b></p>
                            <p>
                                <?php if ($part = explode('_@_', $mail['co']) && !empty($part[1])): ?>
                                    <?= $part[0] ?>
                                    <br><a href="<?= $part[1] ?>">Cliquez ici</a>
                                <?php else: ?>
                                    <?= $mail['co'] ?>
                                <?php endif ?>
                            </p>
                        </div>
                    </section>
                <?php endforeach; ?>
            <?php else: ?>
                <form action="mail.php" method="GET" role="form">
                    <input name="email" type="email" placeholder="email"/>
                    <input type="submit"/>
                </form>
            <?php endif ?>
        </div>
    </div>
</div>