<?php include("inc/header.php");?>

    <section class=" first-section section-padding container" id="intro">
        <div class="row">
            <div class="col-lg-12 ">
                <h1>Information sur le projet</h1>
                <p>Dans le cadre du projet, nous vous proposons de réaliser un site web reposant sur les technologies vues en cours (HTML, CSS, PHP, MySQL). Ce projet nécessite une analyse comme tout projet qui devient conséquent. Ainsi, vous aurez l’occasion d’écrire une application complète. Ce site web devra répondre à toutes les exigences d’une application professionnelle, tant en terme de fonctionnement, que d’ergonomie, de qualité de développement ou de finition. Vous devrez en particulier vous intéresser à l'accessibilité de votre site et à sa portabilité sur d'autres supports.</p>
            </div>
        </div>
    </section>

<?php include('inc/footer.php') ?>