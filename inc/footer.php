<footer class="container-fluid">
    <div class=" row ">
        <div class="footer-object col-xs-12 ">
            <div class=" row ">
                <div class="col-lg-1 col-lg-offset-10">
                    <a href="#" class="footer-object-square">
                        <span class="material-icons floating-arrow arrow-footer">
                            arrow_upward
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row info-ref">
        <div class="footer-info col-sm-8 ">
            <div class="row ">
                <div class="col-xs-offset-2 col-xs-push-10 ">
                    <ul class="footer-group">
                        <li><a href="index.php">Accueil</a></li>
                        <li><a href="info.php">Information</a></li>
                    </ul>
                    <p>Copyright © 2016 <a href="index.php">Gestionnaire Pédagogique des Stages</a>
                        <br>Créé par <a href="mailto:johann.hospice@u-psud.fr">Johann Hospice</a> et Jophilga Dississa.
                    </p>
                </div>
            </div>
        </div>
        <div class="reference col-sm-4">
            <div class="col-lg-12">
                <p>RETROUVEZ LES SITES REFERENTS</p>
                <a href="http://www.iut-orsay.u-psud.fr/fr/index.html" class="iut-orsay">
                    <img class="univ-logo" src="img/icon/orsay.svg" alt="iut orsay">
                </a>
                <a href="http://www.universite-paris-saclay.fr/fr" class="univ-paris-saclay">
                    <img class="univ-logo" src="img/icon/saclay.svg" alt="saclay">
                </a>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/jquery-min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</footer>
</body>
</html>