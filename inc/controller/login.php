<?php
require '../model/bootstrap.php';

$user = App::getUser();
$db = App::getDB();

if (!empty($_POST) && !empty($_POST['login']) && !empty($_POST['password'])) {
    $session = Session::instance();
    if ($user->login($db, $_POST['login'], $_POST['password'])) {
        $session->setFlash('success', 'Vous êtes maintenant connecté');
        App::redirect('account.php');
    }
    $session->setFlash('danger', 'Identifiant ou mot de passe incorrecte');
}
App::redirect('login.php');
