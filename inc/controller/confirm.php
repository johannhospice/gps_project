<?php require '../model/bootstrap.php';

if (!empty($_GET['id']) && !empty($_GET['token'])) {
    if (App::getUser()->confirm(App::getDB(), $_GET['id'], $_GET['token'])) {
        Session::instance()->setFlash('success', 'Votre compte a bien été validé');
        App::redirect('account.php');
    }
    Session::instance()->setFlash("danger", "Ce token est invalide");
    App::redirect('login.php');
}
App::redirect('index.php');