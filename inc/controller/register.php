<?php
require_once '../model/bootstrap.php';

if (!empty($_POST)) {
    $db = App::getDB();
    $session = Session::instance();
    $validator = new Validator($_POST);

    $validator->isMatch('firstname', Validator::PREG_NAME, "Votre prenom est invalide");

    $validator->isMatch('lastname', Validator::PREG_NAME, "Votre nom est invalide");

    $validator->isMatch('phone', Validator::PREG_TELEPHONE, "Votre numéro de téléphone est invalide");

    $validator->isMatch('address', Validator::PREG_ADDRESS, "Votre adresse est invalide");

    $validator->isMatch('postcode', Validator::PREG_NUMBER, "Votre code postale est invalide");

    $validator->isMatch('city', Validator::PREG_NAME, "Votre ville est invalide");

    $validator->isEmail('email', 'Votre email est invalide');

    $validator->isMatch('status',Validator::PREG_STATUS, 'Erreur du formulaire');

    if ($validator->isValid())
        $validator->isUnique('email', $db, 'users', "Votre email est déjà utilisé pour un autre compte");

    $validator->isMatch('password', Validator::PREG_PASSWORD, "Votre mot de passe ne correspond pas ");
    if ($validator->isValid())
        $validator->isPassword('password', "Votre mot de passe est invalide");

    if ($validator->isValid()) {
        if (App::getUser()->register($db, $_POST['password'], $_POST['email'], $_POST['firstname'], $_POST['lastname'], $_POST['status'], $_POST['phone'], $_POST['address'], $_POST['postcode'], $_POST['city'])) {
            $session->setFlash('success', "Un email de confirmation vous a été envoyé par mail, pour valider votre compte");
            App::redirect('login.php');
        } else
            $session->setFlash("danger", "Erreur lors de l'enregistrement de votre compte");
    }
    foreach ($validator->getErrors() as $error)
        $session->setFlash("danger", $error);

}
App::redirect('register.php');
