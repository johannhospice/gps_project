<?php
require_once '../model/bootstrap.php';

if (!empty($_POST) && !empty($_POST['email'])) {
    $db = App::getDB();
    $user = App::getUser();

    $session = Session::instance();
    if ($user->resetPasswordRequest($db, $_POST['email'])) {
        $session->setFlash('success', 'Les instructions du rappel de mot de passe vous ont envoyées par email');
        App::redirect('login.php');
    } else
        $session->setFlash('danger', 'Aucun compte ne correspond à cet adresse');

}
App::redirect('forget.php');
