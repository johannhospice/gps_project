<?php
require '../model/bootstrap.php';
App::getUser()->logout();
App::getDB()->logout();
Session::instance()->setFlash('success', 'Vous êtes maintenant déconnecté');
App::redirect('index.php');
