<?php require '../model/bootstrap.php';
/**
 * @deprecated
 */
if (!empty($_GET['id']) && !empty($_GET['token'])) {
    if ($info = App::getUser()->validate(App::getDB(), $_GET['id'], $_GET['token'])) {
        $mail = new Mail();
        $mail->sendMail($db,'Validation final du compte','Votre compte a bien été validé',$info->email);
        App::redirect('mail.php');
    }
    Session::instance()->setFlash("danger", "Ce token est invalide");
    App::redirect('login.php');
}
App::redirect('index.php');


