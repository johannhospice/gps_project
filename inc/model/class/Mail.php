<?php

/**
 * Class Mail
 * classe permettant de gérer l'envoie de mail
 */
class Mail
{
    /**
     * @var string
     */
    private $sender;

    /**
     * Mail constructor.
     * @param string $sender
     */
    public function __construct($sender = 'contact@gps.fr')
    {
        if (!empty($sender))
            $this->sender = $sender;
    }

    /**
     * Permet d'envoyer un mail
     * @param DataBase $db
     * @param string $receiver
     * @param string $title
     * @param string $corps
     * @return bool|mysqli_result
     */
    public function sendMail($db, $title, $corps, $receiver = 'contact@gps.fr')
    {
        return $db->query("insert into mails set sender='$this->sender', receiver='$receiver', title= '$title', corps='$corps', send_at=now()");
    }

    /**
     * permet de mettre un mail dans un cookie
     * @param DataBase $db
     * @param Cookie $cookie
     */
    public function mailsToCookie($db, $cookie)
    {
        if ($mails = $db->query("select * from mails where receiver = '$this->sender'")) {
            if ($mails->num_rows > 0) {
                $cookie->setCookie("receiver", $this->sender, time() + 5);
                while ($mail = $mails->fetch_object())
                    $cookie->setCookie("mail[$mail->id]", self::mailFormatter($mail->send_at, $mail->sender, $mail->title, $mail->corps), time() + 5);
            }
        }
    }

    /**
     * Permet de segmenter un mail pour pouvoir le faire tenir dans une variable
     * @param $da
     * @param $se
     * @param $ti
     * @param $co
     * @return string
     */
    public static function mailFormatter($da, $se, $ti, $co)
    {
        return $da . '__' . $se . '==' . $ti . '__' . $co;
    }

    /**
     * Permet de découper une varible contenant un mail
     * @param $string
     * @return array
     */
    public static function mailUnformatter($string)
    {
        $part = explode('==', $string);
        $info_mail = explode('__', $part[0]);
        $content_mail = explode('__', $part[1]);
        return array(
            'da' => $info_mail[0],
            'se' => $info_mail[1],
            'ti' => $content_mail[0],
            'co' => $content_mail[1]
        );
    }
}