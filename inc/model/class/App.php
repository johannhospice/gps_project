<?php

/**
 * Class App
 * App est un factory permettant facilitant l'utilisation des autre classe,
 * cette classe gère des fonction englobant l'utilisation de l'application
 */
class App
{

    const STUDENT = 0;
    const TUTOR = 1;
    const PROFESSOR = 2;
    const PROFESSORTUTOR = 3;

    /**
     * @return DataBase
     */
    static function getDB()
    {
        return DataBase::instance();
    }

    private static $user = null;
    /**
     * @return User
     */
    static function getUser()
    {
        if (!self::$user)
            self::$user = new User(Session::instance());
        return self::$user;
    }

    /**
     * Permet d'afficher le contenue d'une variable
     * @param $var
     * @return string
     */
    static function debug($var)
    {
        return '<pre>' . var_dump($var) . '</pre>';
    }

    /**
     * Redirige directement vers une page de la racine
     * @param string $href
     * @param bool $root si le fichier source est a la racine mettre a true sinon, laisser tel quel
     */
    public static function redirect($href, $root = false)
    {
        $root ? header("Location: $href") : header("Location: ../../$href");
        exit();
    }



}