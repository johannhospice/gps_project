<?php

/**
 * Class User
 * Classe gérant un utilisateur
 */
class User
{
    /**
     * @var Session
     */
    private $session;
    /**
     * User constructor.
     * @param Session $session
     * @param array $options
     */
    public function __construct($session, $options = [])
    {
        $this->session = $session;
        $this->options = array_merge_recursive($options);
    }


    /**
     * Création du compte
     * Enregistrement d'un utilisateur dans la base de données
     * @param $db
     * @param $password
     * @param $email
     * @param $firstname
     * @param $lastname
     * @param $status_id
     * @param $phone
     * @param $address
     * @param $postcode
     * @param $city
     * @return bool
     */
    public function register($db, $password, $email, $firstname, $lastname, $status_id, $phone, $address, $postcode, $city)
    {
        $password = Str::password_hash($password);
        $token = Str::str_random(60);

        $firstname = Str::tolower($firstname);
        $lastname = Str::tolower($lastname);
        $address = Str::tolower($address);
        $city = Str::tolower($city);
        $email = Str::tolower($email);
        $login = $firstname . '.' . $lastname;


        if ($deb = $db->query("insert into users set login = '$login', password= '$password', email = '$email',firstname = '$firstname', lastname = '$lastname',status='$status_id', phone = '$phone', address= '$address', postcode= '$postcode',city='$city',confirmation_token='$token'")) {
            $id = $db->getCo()->insert_id;
            $this->sendMail($db, "Confirmation de votre compte", "Afin de valider votre compte, merci de cliquer sur ce lien_@_http://localhost/gps_project/inc/controller/confirm.php?id=$id&token=$token", $email);
            return true;
        }
        return false;
    }


    /**
     * Permet de connecter un utilisateur
     * @param DataBase $db
     * @param string $login
     * @param string $password
     * @return bool|stdClass
     */
    public function login($db, $login, $password)
    {
        $user = $db->query("select * from users where (login = '$login' or email='$login') and confirmed_at is not null")->fetch_object();

        if (Str::password_verify($password, $user->password)) {
            $this->connect($user);
            return $user;
        }
        return false;
    }

    /**
     * Permet de déconnecter un utilisateur
     */
    public function logout()
    {
        Cookie::instance()->setCookie('remember', NULL, -1);
        $this->session->delete('user');
    }

    /**
     * @param stdClass $user
     */
    private function connect($user)
    {
        $this->session->write('user', $user);
    }


    /**
     * Retourne la session de l'utilisateur, (session contenant ses données personnelles)
     * @return bool|mixed|null
     */
    public function user()
    {
        if ($this->session->read('user'))
            return $this->session->read('user');
        return false;
    }

    /**
     * Permet la confirmation de la création du compte
     * @param DataBase $db
     * @param int $id
     * @param string $token
     * @return bool
     */
    public function confirm($db, $id, $token)
    {
        $info = $db->query("select * from users where id=$id")->fetch_object();
        if ($info && $info->confirmation_token == $token) {
            $db->query("update users set confirmation_token= null, confirmed_at = now() WHERE  id = $id");
            $this->connect($info);
            return true;
        }
        return false;
    }

    /**
     * Permet d'envoyer un lien de réinitialisation de mot de passe
     * @param DataBase $db
     * @param string $email
     * @return bool
     */
    public function resetPasswordRequest($db, $email)
    {
        if ($user = $db->query("select * from users where email ='$email' and confirmed_at is not null")->fetch_object()) {
            $token = Str::str_random(60);
            $id = $user->id;
            $db->query("update users set reset_token = '$token', reset_at = NOW() where id=$id");
            $link = "http://localhost/gps_project/reset.php?id=$id&token=$token";
            $this->sendMail($db, "Réinitialisation de votre mot de passe", "Afin de réinitialiser votre mot de passe, merci de cliquer sur ce lien_@_$link", $email);
            return $user;
        }
        return false;
    }

    /**
     * Permet de récuperer un utilisateur souhaitant changer de mot de passe
     * @param DataBase $db
     * @param int $id
     * @param string $token
     * @return stdClass|mixed
     */
    public function resetPasswordConfirmTokenPermission($db, $id, $token)
    {
        return $db->query("select * from users where id=$id and reset_token is not null and reset_token='$token'");
    }
    /**
     * Permet de rediriger un utilisateur vers une page auquelle il a access
     */
    public function restrict()
    {
        if ($this->session->read('user')) {
            $this->session->setFlash('danger', "Vous n'avez pas le droit d'accéder à cette page");
            App::redirect('account.php');
        }
        App::redirect('login.php');
    }

    /**
     * Permet d'envoyer un mail
     * @param DataBase $db
     * @param string $title
     * @param string $msg
     * @param string $receiver
     * @param $sender
     */
    private function sendMail($db, $title, $msg, $receiver, $sender)
    {
        if (empty($sender))
            $mail = new Mail();
        else
            $mail = new Mail($sender);
        $mail->sendMail($db, $title, $msg, $receiver);
    }


    /**
     * Retourne vrai si l'utilisateur à été vérifier par un administrateur
     * @return bool
     * @deprecated
     */
    public function isValid()
    {
        return isset($this->user()->validated_at);
    }

    /**
     * @param DataBase $db
     * @param $id
     * @param $start
     * @param $end
     * @param $com
     * @return bool|mysqli_result
     * @deprecated
     */
    public function disposition($db, $id, $start, $end, $com)
    {
        return $db->query("insert into scheduler set user=$id, start_dispo=$start, end_dispo=$end, comment=$com");
    }

    /**
     * Validation par un admin d'un compte utilisateur
     * @param DataBase $db
     * @param $id
     * @return bool|stdClass
     * @deprecated
     */
    public static function validate($db, $id, $token)
    {
        if ($info = self::verify_token($db, $id, 'confirmation_token', $token)) {
            $db->query("update users set confirmation_token= null, validated_at = now() WHERE  id = $id");
            return $info;
        }
        return false;
    }

    /**
     * Verifi l'existance d'un token chez un utilisateur
     * @param DataBase $db
     * @param $id
     * @param $token
     * @return stdClass|bool
     * @deprecated
     */
    public static function verify_token($db, $id, $token_name, $token)
    {
        $info = $db->query("select * from users where id=$id")->fetch_object();
        return $info && $info->{$token_name} == $token ? $info : false;
    }
}

