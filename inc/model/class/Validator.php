<?php

/**
 * Class Validator
 * Classe permettant de valider différents types d'entré utilisateur
 */
class Validator
{
    private $data;
    private $errors = array();

    const PREG_LOGIN = '/^[a-z]+.[a-z]+$/';
    const PREG_PASSWORD = '/^[a-zA-Z0-9_\.-]{6,18}$/';
    const PREG_EMAIL = '/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
    const PREG_TELEPHONE = '/^[0-9]{10}$/';
    const PREG_WORD = '/^[a-zA-Z]+$/';
    const PREG_NUMBER = '/^[0-9]+$/';
    const PREG_POSTCODE = '/^[0-9]{5}$/';
    const PREG_ADDRESS = '/[a-zA-Z0-9 ]+/';
    const PREG_NAME = '/^[a-zA-Zàèéêëïîùç\'-]+$/';
    const PREG_STATUS = '/^[012]$/';
    const PREG_TEXT = '/^[a-zA-Z]{1,}$/';
    const PREG_DATE = '/^[0-3][1-9]/[0-1][1-12]/[0-9]{4}$/';

    /**
     * Validator constructor.
     * Prends en paramettre le $_POST provennant d'un formulaire à vérifier
     * @param array $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Permet de récuperer la valeur d'un champs si il n'est pas vide
     * @param $field
     * @return null|string
     */
    public function getField($field)
    {
        if (!isset($this->data[$field]))
            return null;
        return $this->data[$field];
    }


    /**
     * Permet de vérifier si un email est valide
     * @param string $field
     * @param string $errorMsg
     */
    public function isEmail($field, $errorMsg)
    {
        if (!filter_var($this->getField($field), FILTER_VALIDATE_EMAIL))
            $errors['$field'] = $errorMsg;
    }

    /**
     * Permet de vérifier si un champs est valide selon une expression réguliere, sinon ajoute une erreur
     * @param string $field
     * @param string $preg
     * @param string $errorMsg
     */
    public function isMatch($field, $preg, $errorMsg)
    {
        if (!preg_match($preg, $this->getField($field)))
            $this->errors[$field] = $errorMsg;
    }

    /**
     * Verifie si un element est unique dans la table de la base de données
     * @param string $field
     * @param DataBase $db
     * @param string $table
     * @param string $errorMsg
     */
    public function isUnique($field, $db, $table, $errorMsg)
    {
        $value = $this->getField($field);
        if ($db->query("select id from $table where $field = '$value'")->fetch_object())
            $this->errors[$field] = $errorMsg;
    }

    /**
     * @param string $field
     * @param string $errorMsg
     */
    public function isPassword($field, $errorMsg = '')
    {
        $this->isMatch($field, Validator::PREG_PASSWORD, "Votre mot de passe est invalide");
        $value = $this->getField($field);
        if (empty($value) || $value != $this->getField($field . '_confirm'))
            $this->errors[$field] = $errorMsg;
    }

    /**
     * Retourne true si il n'y a aucune erreur
     * @return bool
     */
    public function isValid()
    {
        return empty($this->errors);
    }

    /**
     * retourne un tableau d'erreur
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Securise et retourne un champs
     * @param DataBase $db
     * @param $key
     * @return string
     */
    public function secureText($db, $key)
    {
        $string = htmlentities($this->getField($key));
        return $data[$key] = $db->getCo()->real_escape_string($string);
    }
}