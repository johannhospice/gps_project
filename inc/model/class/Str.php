<?php

/**
 * Class Str
 * Classe permettant de gérer des chaines de caractère
 */
class Str
{
    /**
     * Retourne une chaine de caractere aléatoire de la longueur souhaité
     * @param $length
     * @return string
     */
    public static function str_random($length)
    {
        $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
        return substr(str_shuffle(str_repeat($alphabet,$length)),0,$length);
    }

    /**
     * permet de hasher une variable
     * @param $password
     * @return bool|string
     */
    public static function password_hash($password){
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public static function verify_hash($password){
        return password_hash($password, PASSWORD_BCRYPT);
    }
    /**
     * Met une chaine de caractere en minuscule
     * @param $text
     * @return string
     */
    public static function tolower($text)
    {
        return strtolower($text);
    }

    /**
     * Permet de mettre le premier caractere d'une chaine en majuscule et le reste en minuscule
     * @param $text
     * @return string
     */
    public static function str_fm($text)
    {
        return ucfirst($text);
    }

    /**
     * verifie si un hash correspond a un mot de passe
     * @param $password
     * @param $hash
     * @return bool
     */
    public static function password_verify($password,$hash)
    {
        return password_verify($password, $hash);
    }
}