<?php

/**
 * Class DataBase
 * classe permettant de géré la base de donnée
 */
class DataBase
{
    /**
     * @var mysqli
     */
    private $co;

    /**
     * DataBase constructor.
     * @param string $localhost
     * @param string $login
     * @param string $password
     * @param string $db
     */
    private function __construct($localhost = "localhost", $login = "root", $password = "", $db = "db_gps")
    {
        $this->co = new mysqli($localhost, $login, $password, $db);
        if ($this->co->connect_errno) {
            Session::instance()->SetFlash('danger','Echec de la connexion à la base de données');
            App::redirect('index.php');
        }
    }

    /**
     * Fonction permettant de réinitialiser la classe et de se déconnecter de la base de donnée
     */
    public function logout()
    {
        mysqli_close($this->co);
        $db = null;
    }

    /**
     * Retourne la connexion à la base de données
     * @return mysqli
     */
    public function getCo()
    {
        return $this->co;
    }

    /**
     * Execute une requete et renvoie son statement
     * @param string $query
     * @return bool|mysqli_result
     */
    public function query($query)
    {
        return $this->co->query($query);
    }

    /**
     * @var DataBase
     */
    private static $db = null;
    /**
     * Singleton
     * @return DataBase|null
     */
    public static function instance()
    {
        if (!self::$db)
            self::$db = new DataBase();
        return self::$db;
    }
}
