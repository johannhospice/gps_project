<?php

/**
 * Class Session
 * Classe permettant de gérer une session
 */
class Session
{
    /**
     * Session constructor.
     * Démarre une session
     */
    private function __construct()
    {
        session_start();
    }

    /**
     * Permet de mettre dans la session un message flash
     * @param $key
     * @param $msg
     */
    public function setFlash($key, $msg)
    {
        $_SESSION['flash'][$key] = $msg;
    }

    /**
     * Verifie si il existe des messages flash
     * @return bool
     */
    public function hasFlashes()
    {
        return isset($_SESSION['flash']);
    }

    /**
     * récupères tous les méssages flash et les supprimmes
     * @return mixed
     */
    public function getFlashes()
    {
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        return $flash;
    }

    /**
     * insere un variable dans la session avec sa clef en parametre
     * @param $key
     * @param $value
     */
    public function write($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * retourne une variable de la session si elle existe
     * @param $key
     * @return mixed|null
     */
    public function read($key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    /**
     * Supprime la session
     */
    public function destroy()
    {
        session_destroy();
    }

    /**
     * @var Session
     */
    private static $instance = null;

    /**
     * Supprime une variable de la session
     * @param $key
     */
    public function delete($key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * Singleton
     * @return Session
     */
    public static function instance()
    {
        if (!self::$instance)
            self::$instance = new Session();
        return self::$instance;
    }

}