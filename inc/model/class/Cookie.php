<?php

/**
 * Class Cookie
 * Classe permettant de gérer les cookie
 */
class Cookie
{
    /**
     * Cookie constructor.
     */
    private function __construct()
    {
    }

    /**
     * Vérifie si un cookie est présent
     * @param $key
     * @return bool
     */
    public function hasCookie($key)
    {
        return isset($_COOKIE[$key]);
    }

    /**
     * Récupère puis suprime un cookie
     * @param $key
     * @return bool|array
     */
    public function getCookie($key)
    {
        if (isset($_COOKIE[$key])) {
            $cookie = $_COOKIE[$key];
            unset($_COOKIE[$key]);
            setcookie($key, null, -1);
            return $cookie;
        }
        return false;
    }

    /**
     * Créer un cookie
     * @param string $key
     * @param string $value
     * @param int $time
     */
    public function setCookie($key, $value, $time)
    {
        setcookie($key, $value, $time);
    }

    /**
     * Lis un item de cookie et le revoie si il existe sinon retourne null
     * @param $key
     * @return null|array
     */
    public function read($key)
    {
        return isset($_COOKIE[$key]) ? $_COOKIE[$key] : null;
    }

    /**
     * @var Cookie
     */
    private static $instance = null;
    /**
     * Singleton
     * @return Cookie
     */
    public static function instance()
    {
        if (!self::$instance)
            self::$instance = new Cookie();
        return self::$instance;
    }
}