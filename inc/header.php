<?php require_once 'model/bootstrap.php';

$current_file = preg_replace('#^(.+[\\\/])*([^\\\/]+)$#', '$2', $_SERVER['SCRIPT_FILENAME']);
$active_class = "class='navselected'";
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Gestionnaire pédagogique des stages</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="img/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16"/>

    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
<header class="nav-bar">
    <div class="container">
        <nav class="row">
            <div class="col-xs-12  header-nav">

                <a href="index.php" class="logo-header"></a>

                <a href="#" class="hamburger-button" id="hamburger-button">
                    <i class="material-icons">menu</i>
                </a>
                <nav class="navbar-collapse " id="side-bar">
                    <ul class="nav-li-group" id="side-bar-content">
                        <?php if (App::getUser()->user()): ?>
                            <li><a <?= ($current_file == 'account.php') ? $active_class : ' ' ?> href="account.php">Accueil</a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a <?= ($current_file == 'index.php') ? $active_class : '' ?>href="index.php">Accueil</a>
                            </li>
                            <li>
                                <a <?= ($current_file == 'info.php') ? $active_class : '' ?>href="info.php">Information</a>
                            </li>
                        <?php endif; ?>
                        <?php if (Session::instance()->read('user')): ?>
                            <li><a <?= ($current_file == 'profil.php') ? $active_class : '' ?>
                                    href="profil.php">Profile</a></li>
                            <li><a href="inc/controller/logout.php">Déconnexion</a></li>
                        <?php else : ?>
                            <li><a <?= ($current_file == 'login.php') ? $active_class : '' ?>
                                    href="login.php">Connexion</a></li>
                            <li><a <?= ($current_file == 'register.php') ? $active_class : '' ?> href="register.php">Inscription</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
</header>
<?php if (Session::instance()->hasFlashes()) : ?>
    <div class="flash container-fluid">
        <div class="row">
            <?php foreach (Session::instance()->getFlashes() as $type => $message): ?>
                <div class=" col-lg-offset-3 col-lg-6 <?= $type; ?>">
                    <p class="flash-text"><?= $message; ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
