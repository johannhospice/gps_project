<?php require_once 'inc/model/bootstrap.php';
$user = App::getUser();
if (!$user_info = $user->user()) {
    App::redirect("login.php", true);
}
include("inc/header.php"); ?>
    <section class="first-section">
        <section class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            Bonjour <?= Str::str_fm($user_info->firstname) . ' ' . Str::str_fm($user_info->lastname) ?> </h2>
                        <p>Voici la liste de vos opération possible entant <?php
                            if ($user_info->status == 0) echo 'qu\'élève';
                            else if ($user_info->status == 1) echo 'que tuteur';
                            else if ($user_info->status == 2) echo 'qu\'enseignant';
                            else echo 'qu\'enseignant tuteur'; ?> sur votre <b>GPS</b>:</p>

                        <div class="row">
                            <a class="btn  col-xs-12 account-menu-item" href="dispo.php">Saisir
                                disponibilitées</a>

                            <?php if ($user_info->status == App::STUDENT || $user_info->status == App::TUTOR): ?>
                                <a class="btn col-xs-12 account-menu-item" href="form-avis.php">Donner votre
                                    avis sur le stage</a>
                            <?php endif; ?>

                            <?php if ($user_info->status == App::STUDENT): ?>
                                <a class="btn col-xs-12 account-menu-item" href="form-loca.php">Saisir la
                                    fiche de localisation de stage</a>
                            <?php endif; ?>

                            <?php if ($user_info->status == App::PROFESSORTUTOR): ?>
                                <a class="btn  col-xs-12 account-menu-item" href="form-visit.php">remplir le
                                    compte-rendu de visite de stage</a>
                            <?php endif; ?>

                            <?php if ($user_info->status == App::PROFESSOR || $user_info->status == App::PROFESSORTUTOR): ?>
                                <a class="btn col-xs-12 account-menu-item" href="list.php">consulter la liste
                                    des stages et s'inscrire pour encadrer un stagiaire</a>
                            <?php endif; ?>
                        </div>

                    </div>
                    <a class="btn col-sm-offset-8 col-sm-4 account-menu-item" href="chat.php">Envoyer un
                        mail</a>
                </div>
            </div>
            </div>
        </section>
    </section>
<?php include("inc/footer.php"); ?>