# Projet Gestion pédagogique des stages #

Dans le cadre du projet S3, nous vous proposons de réaliser une application reposant sur les technologies vues au cours du semestre et des semestres précédents (Java, HTML, JavaScript, PHP, MySQL) permettant de dématérialiser la gestion pédagogique des stages.
Ce projet nécessite une analyse comme tout projet qui devient conséquent. La partie analyse fera appel au cours de conception orientée objet de S2 et S3. La partie réalisation sera séparée en trois sous applications, la première étant réalisée via le cours de programmation orientée objet de S3, la seconde ayant lieu via le cours de programmation web de S3, la dernière lors du cours de base de données avancée de S3. Les différents rendus (analyse, documents, programmes) feront l'objet d'évaluations spécifiques à chaque cours et compteront dans chacun de ces modules. Ces notes seront ensuite agrégées pour donner la note finale de projet S3.
Cette application devra répondre à toutes les exigences d’une application professionnelle, tant en terme de fonctionnement, que d’ergonomie, de qualité de développement ou de finition.

## Analyse de l'existant ##
La première étape, avant même de commencer à faire une analyse de type UML, est de comprendre quelle est la situation actuelle et quels sont les besoins des utilisateurs. Pour cela, vous avez en annexe les documents existants :

* la fiche de localisation à remplir par le stagiaire avant le début de son stage (document 1),
* la fiche d'avis de l'étudiant sur son stage (document 2),
* la fiche d'appréciation de l'entreprise sur le stagiaire (document 3),
* le compte * *  * rendu de visite de stage (document 4). 

Le fonctionnement actuel est le suivant : l'étudiant, une fois la convention de stage signée, doit remplir le document 1 et le transmettre soit sous forme papier, soit par mail après l'avoir rempli électroniquement. Au cours du stage, l'étudiant et son tuteur en entreprise reçoivent la visite de l'enseignant référent, c'est à cette occasion qu'un entretien avec le tuteur a lieu, et l'enseignant remplit le document 4 avec l'aide du tuteur. Ce document papier est ensuite déposé au secrétariat de 2e année. A la fin de la période de stage, le stagiaire remplit le document 2 qu'il renvoie soit sous forme électronique par email, soit sous forme de document papier qu'il amène le jour de sa soutenance. De même, le tuteur en entreprise remplit le document 3 et la renvoie sous forme électronique ou l'amène le jour de la soutenance.

Bien sûr, il s'agit de la situation idéale, la réalité étant qu'il manque régulièrement un ou plusieurs documents au moment de la soutenance.
Parallèlement à l'envoi du document 1, les enseignants consultent la liste de stagiaires et s'inscrivent sur une liste imprimée pour encadrer tel ou tel étudiant. Cette saisie se fait en deux vagues afin d'éviter aux enseignants non disponibles au moment de l'ouverture de la saisie d’être lésés au niveau du choix.

## Les fonctionnalités à mettre en oeuvre ##
Comme indiqué dans le cahier des charges qui vous a été transmis pour le projet d'analyse, vous devez réaliser un site web qui va permettre la gestion des 