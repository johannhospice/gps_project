<?php require_once 'inc/model/bootstrap.php';
if (App::getUser()->user())
    App::getUser()->restrict();

include("inc/header.php");
?>
    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 center">
                    <h1>Connexion</h1>
                    <h3>Suivez votre <strong>G</strong>estionnaire <strong>P</strong>édagogique des
                        <strong>S</strong>tages
                    </h3>
                    <p>Connectez vous avec vos identifiant de compte GPS</p>

                    <form class="form row" action="inc/controller/login.php" method="POST" role="form">

                        <div class="col-md-12">
                            <div class="input_group">
                                <input class="input_field" type="text" name="login" id="input-field"
                                       required>
                                <label for="login" class="input_label">
                                    <i class="input_label-icon material-icons"
                                       id="input_label-icon">perm_identity</i>
                                    <span class="input_label-content" id="input_label-content">Indentifiant</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="input_group">
                                <input class="input_field" type="password" name="password" id="input-field"
                                       required>
                                <label for="lock" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">lock</i>
                                    <span class="input_label-content" id="input_label-content">Mot de passe</span>
                                </label>
                            </div>
                        </div>

                        <input type="submit" class="btn waves-effect sign-up-btn" value="Connexion"/>
                    </form>

                    <p><a href="forget.php">J'ai oublié mon mot de passe</a></p>
                </div>
            </div>
        </div>
    </section>

<?php include('inc/footer.php') ?>