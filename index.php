<?php require_once 'inc/model/bootstrap.php';
if (App::getUser()->user())
    App::redirect('account.php',true);

include("inc/header.php"); ?>
    <section class="home first-section section-padding">
        <div class="container ">
            <div class="row home-content">
                <img class="home-logo" src="img/logo/logo256.png">
                <h1>Gérer, plannifier, organiser</h1>
                <p>Présentation de “<strong>GPS</strong>”, un site web responsive<br>
                Conçu &amp; développé par Johann Hospice et Jophilga Dississa,<br>
                exclusivement pour l'IUT d'Orsay.</p>
                <a class="arrow-top" href="#intro">
                    <span class="material-icons floating-arrow ">arrow_downward</span>
                </a>
            </div>
        </div>
    </section>
    <section class="container" id="intro">
        <div class="row">
            <div class="col-lg-12 section-padding">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="intro-icon">
                            <span class="material-icons">create</span>
                        </div>
                        <div class="intro-content">
                            <h5>Gestion aisé</h5>
                            <p>Communium sortem studium suum exitio vita ut non iustissimus praeerat negotiis proprium
                                adoritur genere ferire urgente quoque adoritur eum ad.</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="intro-icon">
                            <span class="material-icons">lock_outline</span>
                        </div>
                        <div class="intro-content">
                            <h5>Sécurisé</h5>
                            <p>Cum dimittebat nec dimittebat in ianuis dimittebat venisset ad supervacua festinatis
                                venisset
                                dimittebat principem quas praestrictis perrexit quaedam quaedam causatus.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="intro-icon">
                            <span class="material-icons">accessibility</span>
                        </div>
                        <div class="intro-content">
                            <h5>Accessible</h5>
                            <p>Adiuves potuit quem diligas ad etiam est autem quem adiuves cuique deferre sustinere enim
                                efficere possit Quod Quod possit neque.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="inter section-padding"></section>

    <section class="container">
        <div class="row">
            <div class="col-lg-12 section-padding">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="intro-icon">
                            <span class="material-icons">public</span>
                        </div>
                        <div class="intro-content">
                            <h5>Ouvert</h5>
                            <p>Communium sortem studium suum exitio vita ut non iustissimus praeerat negotiis proprium
                                adoritur genere ferire urgente quoque adoritur eum ad.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="intro-icon">
                            <span class="material-icons">people</span>
                        </div>
                        <div class="intro-content">
                            <h5>partage</h5>
                            <p>Cum dimittebat nec dimittebat in ianuis dimittebat venisset ad supervacua festinatis
                                venisset
                                dimittebat principem quas praestrictis perrexit quaedam quaedam causatus.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="intro-icon">
                            <span class="material-icons">school</span>
                        </div>
                        <div class="intro-content">
                            <h5>Suivie pédagogique</h5>
                            <p>Adiuves potuit quem diligas ad etiam est autem quem adiuves cuique deferre sustinere enim
                                efficere possit Quod Quod possit neque.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include('inc/footer.php') ?>