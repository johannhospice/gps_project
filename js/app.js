(function ($) {
    $('#hamburger-button').click(function (e) {
        e.preventDefault();
        $('#side-bar').toggleClass('side-bar');
        $('#side-bar-content').toggleClass('side-bar-content');
    })
    $('#input-field').focus(function () {
        if (!$(this).val()) {
            $('#input_label-content').toggleClass('input_label-content-out');
            $('#input_label-icon').toggleClass('input_label-icon-out');
        }
    })
    $('#input-field').blur(function () {
        if (!$(this).val()) {
            $('#input_label-content').toggleClass('input_label-content-out');
            $('#input_label-icon').toggleClass('input_label-icon-out');
        }
    })
})(jQuery);