<?php require_once 'inc/model/bootstrap.php';
$user = App::getUser();
$user_info = $user->user();
if (!($user_info->status == App::PROFESSOR || $user_info->status == App::PROFESSORTUTOR))
    $user->restrict();

include("inc/header.php"); ?>
    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 center">
                    <div class="form row" action="" method="POST" role="form">
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include('inc/footer.php') ?>