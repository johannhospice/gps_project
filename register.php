<?php require_once 'inc/model/bootstrap.php';
if (App::getUser()->user())
    $user->restrict();

include("inc/header.php");
?>
    <section class="section-padding first-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 center">

                    <h1>Créer votre compte GPS</h1>
                    <p>Remplissez le formulaire d'inscrtiption</p>

                    <form class="form row" action="inc/controller/register.php" method="POST" role="form">
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="lastname" id="input-field"
                                       required>
                                <label for="lastname" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">account_circle</i>
                                    <span class="input_label-content" id="input_label-content">Nom</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="firstname" id="input-field"
                                       required>
                                <label for="firstname" class="input_label">
                                    <span class="input_label-content" id="input_label-content">Prenom</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input_group">
                                <select class="input_field" name="status">
                                    <option value="<?=App::STUDENT?>">Etudiant</option>
                                    <option value="<?=App::PROFESSOR ?>">Professeur</option>
                                    <option value="<?=App::TUTOR ?>">Tuteur</option>
                                </select>
                                <label for="status" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">person</i>
                                    <span class="input_label-content-select">Status</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="email" name="email" id="input-field"
                                       required>
                                <label for="email" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">email</i>
                                    <span class="input_label-content" id="input_label-content">Email</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="tel" name="phone" id="input-field"
                                       required>
                                <label for="phone" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">phone</i>
                                <span class="input_label-content"
                                      id="input_label-content">Téléphone</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="text" name="address" id="input-field"
                                       required>
                                <label for="login" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">place</i>
                                <span class="input_label-content"
                                      id="input_label-content">Adresse</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="text" name="city" id="input-field"
                                       required>
                                <label for="city" class="input_label">
                                    <span class="input_label-content" id="input_label-content">Ville</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_group">
                                <input class="input_field" type="number" name="postcode" id="input-field"
                                       required>
                                <label for="postcode" class="input_label">
                                    <span class="input_label-content" id="input_label-content">Code Postale</span>
                                </label>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="password" name="password" id="input-field"
                                       required>
                                <label for="login" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">lock</i>
                                <span class="input_label-content"
                                      id="input_label-content">Mot de passe</span>
                                </label>
                                <span class='input_info'>Comporte 8 à 16 caractères</span>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input_group">
                                <input class="input_field" type="password" name="password_confirm" id="input-field"
                                       required>
                                <label for="login" class="input_label">
                                    <i class="input_label-icon material-icons" id="input_label-icon">lock_outline</i>
                                <span class="input_label-content"
                                      id="input_label-content">Mot de passe de confirmation</span>
                                </label>
                                <span class='input_info'>Identique à celui saisie précédement</span>
                            </div>
                        </div>

                        <input type="submit" value="S'inscrire" class="btn  submit-form"/>

                    </form>
                </div>
            </div>
        </div>
    </section>
<?php require 'inc/footer.php'; ?>