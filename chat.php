<?php require_once 'inc/model/bootstrap.php';
$user = App::getUser();
if (!$user_info = $user->user())
    $user->restrict();
if ($_POST['receiver'] && $_POST['msg'] && $_POST['object']) {
    $validator = new Validator($_POST);
    $validator->isMatch('receiver', Validator::PREG_EMAIL, "Votre email destinataire est invalide");
    if ($validator->isValid()) {
        $db = App::getDB();
        $msg = $validator->secureText($db, 'msg');
        $object = $validator->secureText($db, '$object');
        $mail = new Mail($user_info->email);
        if ($mail->sendMail($db, $object, $msg, $_POST['receiver'])) {
            Session::instance()->setFlash('success', 'Votre message a bien été envoyé');
        } else
            Session::instance()->setFlash('danger', 'Erreur rencontré lors de l\'envoie du mail');
        App::redirect('account.php', true);
    }
    Session::instance()->setFlash('danger', 'Votre mail est invalide');
}
include("inc/header.php");
?>
    <section class="first-section">
        <section class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 center">
                        <h1>Mail</h1>
                        <p>Vous pouvez envoyer un mail à l'adresse mail de votre choix dans le cadre du GPS.</p>

                        <form class="form row" action="" method="POST" role="form">
                            <div class="col-sm-12">
                                <div class="input_group">
                                    <input class="input_field" type="text" name="receiver" id="input-field"
                                           required>
                                    <label for="receiver" class="input_label">
                                        <i class="input_label-icon material-icons" id="input_label-icon">contact_mail</i>
                                        <span class="input_label-content" id="input_label-content">Destinataire</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input_group">
                                    <input class="input_field" type="text" name="object" id="input-field"
                                           required>
                                    <label for="object" class="input_label">
                                        <i class="input_label-icon material-icons" id="input_label-icon">mail_outline</i>
                                        <span class="input_label-content" id="input_label-content">Objet</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                                    <label for="msg" class="textarea_label">
                                        <i class="input_label-icon material-icons" id="input_label-icon">mail</i>
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Message</span>
                                    </label>
                                </div>
                            </div>
                            <input type="submit" value="Envoyer" class="btn "/>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </section>
<?php include("inc/footer.php"); ?>