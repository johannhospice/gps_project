<?php require_once 'inc/model/bootstrap.php';

$user = App::getUser();
if (!$user_info = $user->user())
    $user->restrict();


if (!empty($_POST['msg'])) {
    $mail = new Mail($user_info->email);
    $validator = new Validator($_POST);
    $msg =$validator->secureText(App::getDB(),'msg');

    if($mail->sendMail(App::getDB(), "Requête de modification de données personneles id: $user_info->id", $msg))
	    Session::instance()->setFlash("success","Une requète a été envoyé aux administrateur.");
	else
	    Session::instance()->setFlash("danger","Echec de l'envoie de la requête aux administrateur.");
}
include("inc/header.php");
?>
<section class="section-padding first-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Vos informations personnelles</h1>
                <p>Vous avez la possibilitée d'adresser un requete de modification de donnée, qui sera traiter par un
                    administrateur.</p>
                <div class="form-verify row">
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input value="<?= $user_info->lastname ?>" class="input_field disable" type="text"
                                   name="lastname" id="input-field"
                                   disabled>
                            <label for="lastname" class="input_label">
                                <i class="input_label-icon material-icons" id="input_label-icon">account_circle</i>
                                <span class="input_label-content-active disable" id="input_label-content">Nom</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input value="<?= $user_info->firstname ?>" class="input_field disable" type="text"
                                   name="firstname" id="input-field"
                                   disabled>
                            <label for="firstname" class="input_label">
                                    <span class="input_label-content-active disable"
                                          id="input_label-content">Prenom</span>
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input_group ">
                            <select class="input_field disable"
                                    name="status" disabled>
                                <option value="0" <?php if ($user_info->value = 0) echo 'selected' ?>>Etudiant
                                </option>
                                <option value="1" <?php if ($user_info->value = 1) echo 'selected' ?>>Professeur
                                </option>
                                <option value="2" <?php if ($user_info->value = 2) echo 'selected' ?>>Tuteur
                                </option>
                            </select>
                            <label for="status" class="input_label">
                                <i class="input_label-icon material-icons" id="input_label-icon">person</i>
                                <span class="input_label-content-active disable">Status</span>
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input_group">
                            <input value="<?= $user_info->email ?>" class="input_field disable" type="email"
                                   name="email" id="input-field"
                                   disabled>
                            <label for="email" class="input_label">
                                <i class="input_label-icon material-icons" id="input_label-icon">email</i>
                                    <span class="input_label-content-active disable"
                                          id="input_label-content">Email</span>
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input_group">
                            <input value="<?= $user_info->phone ?>" class="input_field disable" type="tel"
                                   name="phone"
                                   id="input-field"
                                   disabled>
                            <label for="phone" class="input_label">
                                <i class="input_label-icon material-icons" id="input_label-icon">phone</i>
                                <span class="input_label-content-active disable"
                                      id="input_label-content-active disable">Téléphone</span>
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input_group">
                            <input value="<?= $user_info->address ?>" class="input_field disable" type="text"
                                   name="address" id="input-field"
                                   disabled>
                            <label for="login" class="input_label">
                                <i class="input_label-icon material-icons" id="input_label-icon">place</i>
                                <span class="input_label-content-active disable"
                                      id="input_label-content-active ">Adresse</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input value="<?= $user_info->city ?>" class="input_field disable" type="text"
                                   name="city"
                                   id="input-field"
                                   disabled>
                            <label for="city" class="input_label">
                                    <span class="input_label-content-active disable"
                                          id="input_label-content">Ville</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input value="<?= $user_info->postcode ?>" class="input_field disable" type="number"
                                   name="postcode" id="input-field"
                                   disabled>
                            <label for="postcode" class="input_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Code Postale</span>
                            </label>
                        </div>
                    </div>
                </div>
                
                <form class="form row" action="" method="POST" role="form">
                    <p>Saisissez les motivations de la modification de vos données</p>
                    <div class="col-sm-12">
                        <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                            <label for="msg" class="textarea_label">
                                <i class="input_label-icon material-icons" id="input_label-icon">mail_outline</i>
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Motivations</span>
                            </label>
                        </div>
                    </div>
                    <input type="submit" value="Envoyer une requête" class="btn"/>
                </form>
            </div>
        </div>
    </div>
</section>
<?php include('inc/footer.php'); ?>
