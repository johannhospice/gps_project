<?php require_once 'inc/model/bootstrap.php';
$user = App::getUser();
$user_info = $user->user();
if (!($user_info->status == App::PROFESSORTUTOR))
    $user->restrict();

include("inc/header.php");
?>

<section class="section-padding first-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 center">
                <h1>Localisation du stage</h1>
                <form class="form row" action="" method="POST" role="form">
                    <h2>Entreprise</h2>
                    <div class="col-sm-12">
                        <div class="input_group">
                            <input class="input_field" type="text" name="name_ent" id="input-field"
                                   required>
                            <label for="name_ent" class="input_label">
                                <span class="input_label-content" id="input_label-content">Raison sociale de l'entreprise</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group">
                            <input class="input_field" type="text" name="address_ent" id="input-field"
                                   required>
                            <label for="address_ent" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Adresse de l'entreprise</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input class="input_field" type="text" name="name_ent" id="input-field"
                                   required>
                            <label for="name_ent" class="input_label">
                                <span class="input_label-content" id="input_label-content">Code postale de l'entreprise</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input class="input_field" type="text" name="name_ent" id="input-field"
                                   required>
                            <label for="name_ent" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Ville de l'entreprise</span>
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input_group ">
                            <select class="input_field "
                                    name="rem" required>
                                <option value="1">Oui</option>
                                <option value="0">Non</option>
                            </select>
                            <label for="status" class="input_label">
                                <span class="input_label-content-active">Vous êtes vous rendu sur le lieu du stage pour rencontrer le stagiaire et son responsable de stage ? </span>
                            </label>
                        </div>
                    </div>

                    <h2>Responsable</h2>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input class="input_field" type="text" name="firstname_resp" id="input-field"
                                   required>
                            <label for="name_ent" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Prenom du responsable</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input class="input_field" type="text" name="lastname_resp" id="input-field"
                                   required>
                            <label for="name_resp" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Nom du responsable</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group">
                            <input class="input_field" type="text" name="func_resp" id="input-field"
                                   required>
                            <label for="name_ent" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Fonction du responsable</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input class="input_field" type="text" name="email_resp" id="input-field"
                                   required>
                            <label for="firstname_tutor" class="input_label">
                                <span class="input_label-content" id="input_label-content">Email</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input_group">
                            <input class="input_field" type="text" name="email_resp" id="input-field"
                                   required>
                            <label for="firstname_tutor" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Numero de téléphone</span>
                            </label>
                        </div>
                    </div>

                    <h2>Encadrement</h2>
                    <div class="col-sm-12">
                        <div class="input_group ">
                            <select class="input_field "
                                    name="rem" required>
                                <option value="1">Oui</option>
                                <option value="0">Non</option>
                            </select>
                            <label for="status" class="input_label">
                                <span class="input_label-content-active">Avez-vous été encadré directement par un informaticien</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group ">
                            <select class="input_field "
                                    name="rem" required>
                                <option value="1">Oui</option>
                                <option value="0">Non</option>
                            </select>
                            <label for="status" class="input_label">
                                <span class="input_label-content-active">En cas de besoin pouviez-vous faire appel à un informaticien</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group ">
                            <select class="input_field "
                                    name="rem" required>
                                <option value="1">Oui</option>
                                <option value="0">Non</option>
                            </select>
                            <label for="status" class="input_label">
                                <span class="input_label-content-active">Dans le cadre de votre stage,avez-vous travaillé seul ?</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group">
                            <input class="input_field" type="text" name="rem_value" id="input-field"
                                   required>
                            <label for="name_resp" class="input_label">
                                    <span class="input_label-content"
                                          id="input_label-content">Taille de l'équipe</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group">
                            <fieldset>
                                <legend>Taille de l'équipe</legend>
                                <div>
                                    <input type="checkbox" value="0">PC</input>
                                    <input type="text" placeholder="AUTRES"/>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group">
                            <fieldset>
                                <legend>Système</legend>
                                <div>
                                    <input type="checkbox" value="0">UNIX</input>
                                    <input type="checkbox" value="1">LINUX</input>
                                    <input type="checkbox" value="2">WINDOWS</input>
                                    <input type="text" placeholder="AUTRES"/>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                            <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Langages</span>
                            </label>
                        </div>
                    </div>
                    <h2>Objet principal du stage</h2>
                    <p>2 cases max.</p>
                    <div class="col-sm-12">
                        <div class="input_group">
                            <fieldset>
                                <legend>Système</legend>
                                <div>
                                    <input type="checkbox" value="0">Systeme</input>
                                    <input type="checkbox" value="1">Multimedia</input>
                                    <input type="checkbox" value="2">Réseaux</input>
                                    <input type="checkbox" value="3">Développement WEB</input>
                                    <input type="checkbox" value="4">Aute Développement</input>
                                    <input type="checkbox" value="5">Bases de données</input>
                                    <input type="text" placeholder="AUTRES"/>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input_group ">
                            <select class="input_field "
                                    name="rem" required>
                                <option value="0">Systeme</option>
                                <option value="1">Multimédia</option>
                                <option value="2">Réseaux</option>
                                <option value="3">Développement WEB</option>
                                <option value="4">Autre Développement</option>
                                <option value="5">Base de données</option>
                            </select>
                            <label for="status" class="input_label">
                                <span class="input_label-content-active">Dans le cadre de votre stage,avez-vous travaillé seul ?</span>
                            </label>
                        </div>
                    </div>

                    <h2>Avis De l'entreprise sur le travail et le compotement de l'étufiant</h2>
                    <div class="col-sm-12">
                        <div class="input_group ">
                            <select class="input_field "
                                    name="rem" required>
                                <option value="0">Pas satisfait</option>
                                <option value="1">Peu satisfait</option>
                                <option value="2">Satisfait</option>
                                <option value="3">Très satisfait</option>
                            </select>
                            <label for="status" class="input_label">
                                <span class="input_label-content-active">Dans le cadre de votre stage,avez-vous travaillé seul ?</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                            <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Commentaires</span>
                            </label>
                        </div>
                    </div>
                    <h2>Avis de l'entreprise sur la formation recue a l'iut</h2>
                    <div class="col-sm-12">
                        <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                            <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Dans la formation telle que vous l’avez perçue lors de la présence du stagiaire dans votre entreprise, avez-vous constaté des manques handicapants pour un futur informaticien ?</span>
                            </label>
                        </div>
                    </div>

                    <h2>Avis général de l'neseignant sur le stage</h2>
                    <div class="col-sm-12">
                        <div class="input_group">
                                <textarea rows="100" class="input_field textarea_field" name="msg" id="textarea_field "
                                          required></textarea>
                            <label for="msg" class="textarea_label">
                                <span class="input_label-content-active disable"
                                      id="input_label-content">Précisez en quelques lignes comment le stage a enrichi ou modifié votre projet personnel et professionnel</span>
                            </label>
                        </div>
                    </div>

                    <input type="submit" value="soumettre" class="btn"/>

                </form>
            </div>
        </div>
    </div>
</section>

<?php include('inc/footer.php') ?>